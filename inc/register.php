<div class="row">
	<div class="col-md-4"></div>
	<div class="col-md-4" style="top:10px">
            <h1>REGISTRACIJA</h1>
            <form role="form" id="registration" onsubmit="return registerValidate();" method="post">
                        <div class="form-group">
				<label for="first_name">Ime</label>
				<input type="text" class="form-control" name="first_name" id="first_name" placeholder="Vpišite vaše ime" />
			</div>
                        <div class="form-group">
				<label for="last_name">Priimek</label>
				<input type="text" class="form-control" name="last_name" id="last_name" placeholder="Vpišite vaš priimek" />
			</div>
                        <div class="form-group">
				<label for="street_address">Ulica in hišna številka</label>
				<input type="text" class="form-control" name="street_address" id="street_address" placeholder="Vpišite ime ulice in hišno številko" />
			</div>
                        <div class="form-group">
				<label for="postal_code">Poštna številka</label>
				<input type="text" class="form-control" name="postal_code" id="postal_code" placeholder="Vpišite vašo poštno številko" />
			</div>
                        <div class="form-group">
				<label for="city">Kraj</label>
				<input type="text" class="form-control" name="city" id="city" placeholder="Vpišite kraj" />
			</div>
                        <div class="form-group">
				<label for="email">Elektronski naslov</label>
				<input type="text" class="form-control" name="email" id="email" placeholder="Vpišite vaš elektronski naslov" />
			</div>
			<div class="form-group">
				<label for="username">Uporabniško ime</label>
				<input type="text" class="form-control" name="username" id="username" placeholder="Vpišite vaše uporabniško ime" />
			</div>
                        <div class="form-group">
				<label for="phone_number">Telefonska številka</label>
				<input type="text" class="form-control" name="phone_number" id="phone_number" placeholder="Vpišite vašo tel. št." />
			</div>
			<div class="form-group">
				<label for="password">Geslo</label>
				<input type="password" class="form-control" name="password" id="password" onkeypress="Check(this.id, 6);"  placeholder="Vpišite geslo" />
			</div>
                        <div class="form-group">
				<label for="password">Ponovite geslo</label>
				<input type="password" class="form-control" name="password-repeat" id="password-repeat"  placeholder="Vpišite geslo" />
			</div>
                        <div class="form-group">
                            <div class="g-recaptcha" data-sitekey="6Lf-DxUTAAAAAKJCPJ6_xJbLnot7nxJtt-1gQrhR"></div>
                        </div>
                      
                        <div class="form-group">
                        <div class="notification"></div>	
                        </div> 
			<div class="form-group">
                            <button type="submit" class="btn btn-default">Potrdi registracijo</button>
                    </div>
            </form>
				
	</div>
	<div class="col-md-4"></div>
</div>
