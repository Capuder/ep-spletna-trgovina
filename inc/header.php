<header id="section_header" class="navbar-fixed-top main-nav" role="banner">
            <div class="footer_b">
        <div class="">
            <div class="row">
                <div class="col-md-8"></div>
                <div class="col-md-4">
                    <div class="user_meni">
                        <?php
                           if (isset($_SESSION['username'])){
                                echo '| <a href="/racun">Moj račun</a> ';
                                echo '| <a href="/kosarica">Moja košarica</a> ';
                                echo '| <a href="/modifycustomer">Moji podatki</a> ';
                                echo '| <a href="/changepass">Spremeni geslo</a>';
                                echo '| <a href="/logout">Odjava</a>';
                                echo '| <a href="/racun" >Uporabnik: <b>' . $_SESSION['username'] . '</b></a>';
                           } else{
                                echo '| <a href="/login">Prijava</a> ';
                                echo '| <a href="/register">Registracija</a>';
                                echo '| <a href="/kosarica">Moja košarica</a> ';
                           }
                        ?>
                        </p>
                    </div>
                </div>
            </div>
        </div>
    </div>
        <div class="container">
            <!-- <div class="row"> -->
                 <div class="navbar-header ">
                     <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                            <span class="sr-only">Toggle navigation</span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                        </button>
                        <!--a class="navbar-brand" href="#">
                            <img src="images/logo2.png" alt="">
                        </a-->
                 </div><!--Navbar header End-->
                    <nav class="collapse navbar-collapse navigation" id="bs-example-navbar-collapse-1" role="navigation">
                        <ul class="nav navbar-nav navbar-right ">
                            <li><a href="/">Domov </a> </li>
                            <li>
                             <div class="dropdown">
                                <a class="dropdown-toggle" type="button" data-toggle="dropdown">Kolesa <span class="glyphicon glyphicon-chevron-down" aria-hidden="true"></span></a>
                                <ul class="dropdown-menu">
                                  <li><a href="otroska-kolesa">Otroška kolesa</a></li>
                                  <li><a href="gorska-kolesa">Gorska kolesa</a></li>
                                  <li><a href="cestna-kolesa">Cestna kolesa</a></li>
                                </ul>
                             </div>
                            </li>
                            <li><a href="/kontakt">Kontakt</a> </li>
                        </ul>
                     </nav>
                </div><!-- /.container-fluid -->
</header>