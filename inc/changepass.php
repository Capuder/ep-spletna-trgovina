<?php
include 'checkcustomer.php';
?>

<div class="row">
    <div class="col-md-4"></div>
    <div class="col-md-4" style="top:10px">
            <h1>SPREMEMBA GESLA</h1>     
       
            <div class="form-group">
                <label for="curpassword">Trenutno geslo</label>
                <input type="password" class="form-control" name="curpassword" id="curpassword" placeholder="Vpišite vaše trenutno geslo" />
            </div>
            <div class="form-group">
                <label for="password">Novo Geslo</label>
                <input type="password" class="form-control" name="password" id="password" placeholder="Vpišite novo geslo" />
            </div>
            <div class="form-group">
				<label for="password">Ponovite novo geslo</label>
				<input type="password" class="form-control" name="password-repeat" id="password-repeat"  placeholder="Ponovite novo geslo" />
            </div>
            <div class="form-group">
                <button class="btn btn-default" onclick="AjaxCall()">Potrdi</button>
            </div>
            <div class="notification"></div>
           
    </div>
    <div class="col-md-4"></div>
</div>   

<script>
    function AjaxCall(){
            $.ajax({
							method: "POST",
							url: "../api/user_changepw",                                                        
							data: { 
                                                            curpassword: $('#curpassword').val(),
                                                            password: $('#password').val()
                                                        },
							success: function(data){
								$('div.notification').text(data.responseText);
							}, 
							error: function(data){
                                                               $('div.notification').text(data.responseText); 
							}
							});
                                                        
                                                    };
                                                     
</script>