<section id="footer">
    <div class="footer_b">
        <div class="container">
            <div class="row">
                <div class="col-md-6 col-sm-6 col-xs-12">
                    <div class="footer_bottom">
                        <p class="text-block"> &copy; <span>Elektronsko poslovanje 2015/2016 </span>
                        <?php
                           if (isset($_SESSION['username'])){
                                echo ' <a href="/logout">Odjava</a>';
                           } else{
                                echo '|  <a href="/login">Prijava</a> | ';
                                echo'<a href="/register">Registracija</a>';
                           }
                        ?>
                        </p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
