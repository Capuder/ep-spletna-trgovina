<div>blablabla</div>
<?php
$absPath = $_SERVER['QUERY_STRING'];
if (strpos($absPath,'gorska-kolesa'))
         $parameters['id_category'] = 0;
else if (strpos($absPath,'cestna-kolesa'))
         $parameters['id_category'] = 1;
else if (strpos($absPath,'otroska-kolesa'))
         $parameters['id_category'] = 2;

$products = ProductDB::getAllByCategory($parameters);
?>
<div class="container" style="margin-top:150px">
    <div class="well well-sm">
        <strong>Prikaz</strong>
        <div class="btn-group">
            <a href="#" id="list" class="btn btn-default btn-sm"><span class="glyphicon glyphicon-th-list">
            </span>Seznam</a> <a href="#" id="grid" class="btn btn-default btn-sm"><span
                class="glyphicon glyphicon-th"></span>Mreža</a>
        </div>
    </div>
    <div id="products" class="row list-group">
       <?php foreach($products as $p){ ?>
        <div class="item  col-xs-4 col-lg-4">
            <div class="thumbnail">
                <a href="izdelek?id=<?php echo $p['id_product'] ?>"><img class="group list-group-image" src="<?php echo $p['path'] ?>" alt="" /></a>
                <div class="caption">
                    <a href="izdelek?id=<?php echo $p['id_product'] ?>"><h4 class="group inner list-group-item-heading">
                        <?php echo $p['product_name'] ?></h4></a>
                    <p class="group inner list-group-item-text">
                        <?php echo $p['short_description'] ?>.</p>
                    <div class="row">
                        <div class="col-xs-12 col-md-6">
                            <p class="lead">
                                <?php echo $p['short_description'] ?></p>
                        </div>
                        <div class="col-xs-12 col-md-6">
                            <a class="btn btn-success" id="add_to_basket" id_product="<?php echo $p['id_product'] ?>" href="javascript:void(0)">Dodaj v košarico</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
       <?php } ?>
    </div>                  
</div>       