<?php
$params['id_product'] =  $_GET['id'];
$p = ProductDB::get($params)[0];

?>
<div class="container" style="margin-top:150px">
<div id="products" class="row list-group">
               <div class="item  col-xs-4 col-lg-4 list-group-item">
            <div class="thumbnail">
                <img class="group list-group-image" src="<?php echo $p['path']; ?>" style="max-height:500px;" alt="">
                <div class="caption">
                    <h4 class="group inner list-group-item-heading">
                        <?php $p['product_name'] ?></h4> 
                    <p class="group inner list-group-item-text">
                        CENA: <?php $p['price'] ?>€</p>
                    <div class="row">
                        <div class="col-xs-12 col-md-6">
                            <p class="lead">
                                <?php echo $p['description']; ?></p>
                        </div>
                        <div class="col-xs-12 col-md-6">
                            <a class="btn btn-success" id="add_to_basket" id_product="<?php echo $p['id_product'] ?>" href="javascript:void(0)">Dodaj v košarico</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>