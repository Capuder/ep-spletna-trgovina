<?php
// if already loged in redirect to index
  if (isset($_SESSION['username'])){
      header('Location: ' . $siteurl);
      die();      
  }   
?>

<div class="row">
    <div class="col-md-4"></div>
    <div class="col-md-4" style="top:10px">
            <h1>PRIJAVA</h1>     
       
            <div class="form-group">
                <label for="username">Uporabniško ime</label>
                <input type="text" class="form-control" name="username" id="username" placeholder="Vpišite vaše uporabniško ime" />
            </div>
            <div class="form-group">
                <label for="password">Geslo</label>
                <input type="password" class="form-control" name="password" id="password" placeholder="Vpišite geslo" />
            </div>
                      
                        <div class="form-group">
                        <div class="notification"></div>	
                        </div> 
            <div class="form-group">
                <button class="btn btn-default" onclick="AjaxCall()">Potrdi</button>
            </div>
          
        
        <a href="/forgotpass">Pozabljeno geslo</a>
        
        
           
    </div>
    <div class="col-md-4"></div>
</div>   

<script>
    function AjaxCall(){
    var un = $('#username').val();
    var pw = $('#password').val();
   
            $.ajax({
							method: "POST",
							url: "api/user",                                                        
							data: { username: un, password: pw},
							success: function(data){
								$('div.notification').text(data.responseText);      
                                                               if(data['status'] === 200){
                                                                   window.location = "/"; 
                                                               }
							}, 
							error: function(data){
                                                               $('div.notification').text(data.responseText);      
                                                               if(data['status'] === 200){
                                                                   window.location = "/"; 
                                                               }
							}
							});
                                                        
                                                    };
                                                     
</script>