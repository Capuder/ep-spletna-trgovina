<?php
function shuffle_assoc(&$array) {
	$keys = array_keys($array);
	shuffle($keys);

	foreach($keys as $key) {
		$new[$key] = $array[$key];
	}

	$array = $new;

	return true;
}

function contains_value($string, array $array) {

    foreach($array as $value) {
        if (strpos($string,$value)) return $value;
    }

    return '';
}