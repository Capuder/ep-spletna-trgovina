<?php
if (isset($_SESSION['basket'])){
    $pids = '';
    $price = 0;
    $postage = 3.5;
    $products = array();
    $p_order = $_SESSION['basket'];
    foreach ($p_order as $pid){
        $params['id_product'] = $pid;
        $pids .= $pid . '_';
        $p = ProductDB::get($params);
        array_push($products, $p);
    }
    var_dump($products);
}
?>
<div class="container">
    <div class="row" style="margin-top:100px">
        <div class="col-sm-12 col-md-10 col-md-offset-1">
            <table class="table table-hover">
                <thead>
                    <tr>
                        <th>Izdelek</th>
                        <!--th>Količina</th-->
                        <th class="text-center">Cena</th>
                        <!--th class="text-center">Skupaj</th-->
                        <th> </th>
                    </tr>
                </thead>
                <tbody>
                    <?php foreach ($products as $p){ $price += $p[0]['price']?>
                    <tr>
                        <td class="col-sm-8 col-md-6">
                        <div class="media">
                            <a class="thumbnail pull-left" href="#"> <img class="media-object" src="http://icons.iconarchive.com/icons/custom-icon-design/flatastic-2/72/product-icon.png" style="width: 72px; height: 72px;"> </a>
                            <div class="media-body">
                                <h4 class="media-heading"><a href="#"><?php echo $p[0]['product_name']; ?></a></h4>
                                <h5 class="media-heading"> znamka <a href="#"><?php $p[0]['manufacturer'] ?></a></h5>
                                <span>Status: </span><span class="text-success"><strong>Na zalogi</strong></span>
                            </div>
                        </div></td>
                        <!--td class="col-sm-1 col-md-1" style="text-align: center">
                        <input type="email" class="form-control" id="exampleInputEmail1" value="3">
                        </td-->
                        <td class="col-sm-1 col-md-1 text-center"><strong><?php echo $p[0]['price'] ?></strong></td>
                        <!--td class="col-sm-1 col-md-1 text-center"><strong></strong></td-->
                        <td class="col-sm-1 col-md-1">
                        <button type="button" class="btn btn-danger">
                            <span class="glyphicon glyphicon-remove"></span> Odstrani
                        </button></td>
                    </tr>
                   <?php } ?>
                    <tr>
                        <td>   </td>
                        <td>   </td>
                        <td>   </td>
                        <td><h5>Skupaj</h5></td>
                        <td class="text-right"><h5><strong><?php echo $price; ?>€</strong></h5></td>
                    </tr>
                    <tr>
                        <td>   </td>
                        <td>   </td>
                        <td>   </td>
                        <td><h5>Poštnina</h5></td>
                        <td class="text-right"><h5><strong><?php echo $postage; ?>€</strong></h5></td>
                    </tr>
                    <tr>
                        <td>   </td>
                        <td>   </td>
                        <td>   </td>
                        <td><h3>Skupaj</h3></td>
                        <td class="text-right"><h3><strong>2103.50€</strong></h3></td>
                    </tr>
                    <tr>
                        <td>   </td>
                        <td>   </td>
                        <td>   </td>
                        <td>
                        <button type="button" class="btn btn-default">
                            <span class="glyphicon glyphicon-shopping-cart"></span> Nadaljuj z nakupovanjem
                        </button></td>
                        <td>
                        <a href="<?php echo 'add-order?ids' . $pids ?>>< class="btn btn-success">
                            Oddaj naročilo <span class="glyphicon glyphicon-play"></span>
                        </a></td>
                    </tr>
                </tbody>
            </table>
        </div>
    </div>
</div>