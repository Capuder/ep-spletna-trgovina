<?php
    require_once './db/UserDB.php';
    require_once './db/ProductDB.php';
    require_once './inc/functions.php';
    require_once './inc/config.php';
    $path = str_replace('-', '_', contains_value($absPath, $paths));
    if (strpos($path, 'kolesa')) $path = 'kolesa';

?>	
		<!DOCTYPE html>
		<html>
		<head>
			<?php include 'inc/head.php'; ?>
		</head>

		 <body data-target=".navbar-fixed-top">
			<?php 
				include 'inc/header.php';
					$content = 'inc/intro.php';

					if ($path !== '') $content = 'inc/' . $path . '.php';

					 include $content;
					 include 'inc/footer.php';
					 include 'inc/scripts.php'; 
			 ?>
		 </body>
		</html>
		
<?php
?>