<?php

require_once '../db/ProductDB.php';
require_once '../db/UserDB.php';
require_once '../db/CategoryDB.php';
require_once '../db/ImageDB.php';
require_once '../inc/recaptcha_lib.php';
session_start();

header('Content-Type: application/json');

$siteurl = 'http://' . $_SERVER['HTTP_HOST'];
$http_method = filter_input(INPUT_SERVER, "REQUEST_METHOD", FILTER_SANITIZE_SPECIAL_CHARS);
$server_addr = filter_input(INPUT_SERVER, "SERVER_ADDR", FILTER_SANITIZE_SPECIAL_CHARS);
#$server_addr = "10.0.2.2"; // kadar dostopamo preko Android emulatorja
$php_self = filter_input(INPUT_SERVER, "PHP_SELF", FILTER_SANITIZE_SPECIAL_CHARS);
$script_uri = substr($php_self, 0, strripos($php_self, "/"));
$request = filter_input(INPUT_GET, "request", FILTER_SANITIZE_SPECIAL_CHARS);

$rules = array(
    'username' => FILTER_SANITIZE_STRING,
    'password' => FILTER_SANITIZE_SPECIAL_CHARS,
    'first_name' => FILTER_SANITIZE_STRING,
    'last_name' => FILTER_SANITIZE_STRING,
    'street_address' => FILTER_SANITIZE_STRING,
    'email' => FILTER_SANITIZE_EMAIL,
    'phone_number' => FILTER_SANITIZE_NUMBER_INT,
    'postal_code' => FILTER_SANITIZE_NUMBER_INT,
    'city' => FILTER_SANITIZE_STRING,
    'curpassword' => FILTER_SANITIZE_SPECIAL_CHARS,
);


function returnError($code, $message) {
    http_response_code($code);
    echo json_encode($message);
    exit();
}

if ($request != null) {
    $path = explode("/", $request);
} else {
    returnError(400, "Missing request path.");
}

if (isset($path[0])) {
    $resource = $path[0];
} else {
    returnError(400, "Missing resource.");
}

if (isset($path[1])) {
    $param = $path[1];
} else {
    $param = null;
}

switch ($resource) {
    case "categories":
        if ($http_method == "GET" && $param == null) {
            $categories = CategoryDB::getAll();          
            echo json_encode($categories);
        } 
        break;
    case "products":
        if ($http_method == "GET" && $param == null) {
            // getAll
            $products = ProductDB::getAll(); 
            echo json_encode($products);
        } else if ($http_method == "GET" && $param != null) {
            // get
            $products = ProductDB::get(["id_product" => $param]);

            if ($products != null) {
                $products = $products[0];
                echo json_encode($products);
            } else {
                returnError(404, "No entry for id: " . $param);
            }
        }         
        break;
      case "products_cat":
        if ($http_method == "GET" && $param == null) {
            returnError(404, "No category id specified");
        } else if ($http_method == "GET" && $param != null) {
            // get
            $products = ProductDB::getAllByCategory(["id_category" => $param]);

            if ($products != null) {
                echo json_encode($products);
            } else {
                returnError(404, "No entry for category id: " . $param);
            }
        }         
        break;
      case "products_img":
        if ($http_method == "GET" && $param == null) {
            returnError(404, "No product id specified");
        } else if ($http_method == "GET" && $param != null) {
            $imgs = ImageDB::get(["id_product" => $param]);            
            
            if ($imgs != null) {
                for ($x = 0; $x < count($imgs); $x++) {
                     $imgs[$x]['path'] = str_replace("../..", $siteurl, $imgs[$x]['path']);
                } 
                echo json_encode($imgs);
            } else {
                returnError(404, "No entry for product id: " . $param);
            }
        }         
        break;
    case "logout":
        if ($http_method == "GET") {
            try{
                session_unset();
                session_destroy();
              //  if($param == null){
                    http_response_code(201);
                    echo "Logged out.";
            /*    }
                else{
                    header("location:index.php");
                    exit();
                }*/
            } catch (Exception $exc) {
                echo returnError(400, $exc->getMessage());
            } 
        } 
        break;
        
    case "customermodify":
         if ($http_method == "POST" && $param == null){
             try{
            $filteredInput = filter_input_array(INPUT_POST, $rules);
            $data = array_filter($filteredInput);    
            
            $customer = array();
            $customer['street_address'] = $data['street_address'];
            $customer['city'] = $data['city'];
            $customer['postal_code'] = $data['postal_code'];
            $customer['phone_number'] = $data['phone_number'];
            $customer['userid'] = $_SESSION['userid'];
            UserDB::APIUpdateCustomer($customer);
            } catch (Exception $exc) {
                echo returnError(400, $exc->getMessage());
            }          
         }
    case "usermodify":
         if ($http_method == "POST" && $param == null){             
            try{
            $filteredInput = filter_input_array(INPUT_POST, $rules);
            $data = array_filter($filteredInput);    
            
            $user = array();
            $user['first_name'] = $data['first_name'];
            $user['last_name'] = $data['last_name'];
            $user['email'] = $data['email'];
            $user['userid'] = $_SESSION['userid'];
            UserDB::APIUpdateUser($user);
            
            echo 'Podatki shranjeni!';
             http_response_code(201);
            } catch (Exception $exc) {
                echo returnError(400, $exc->getMessage());
            }        
         } 
         break;
    case "user_changepw":
         if ($http_method == "POST" && $param == null){             
            try{
            $filteredInput = filter_input_array(INPUT_POST, $rules);
            $data = array_filter($filteredInput);            
                        
            $params = array();
            $params['username'] = $_SESSION['username'];
            $params['password'] = hash("sha512", $data['curpassword']);
            $checklogin = UserDB::ApiLoginCheck($params);  
            if(array_key_exists(0, $checklogin) >= 1){
               $newpw = array();
               $newpw['password'] = hash("sha512", $data['password']);
               $newpw['userid'] = $_SESSION['userid'];
               $result = UserDB::APIUpdatePassword($newpw);
               if($result == "0"){
                   echo "Password changed!";
               }
                else {
                    echo "Wrong current password.";
                    
                }
            }
            else{
                echo "Wrong current password.";
            }
             http_response_code(201);
            } catch (Exception $exc) {
                echo returnError(400, $exc->getMessage());
            }  
         }
        break;
    case "user_reg":
        if ($http_method == "POST" && $param == null){
            $filteredInput = filter_input_array(INPUT_POST, $rules);
            $user = array_filter($filteredInput);         
            $user['password'] = hash("sha512", $user['password']);
            
            $cuc = array();
            $cuc['idrole'] = 3;
            $cuc['username'] = $user['username'];
            $cuc['password'] = $user['password'];
            $cuc['first_name'] = $user['first_name'];
            $cuc['last_name'] = $user['last_name'];
            $cuc['email'] = $user['email'];
            
            $secret = "6Lf-DxUTAAAAAE8OskFp4lHBtSupoCwirFawXDrF";
            $response = null;
            $reCaptcha = new ReCaptcha($secret);
            
            if ($_POST["g-recaptcha-response"]) {
                $response = $reCaptcha->verifyResponse(
                $_SERVER["REMOTE_ADDR"],
                $_POST["g-recaptcha-response"]);
                if ($response != null && $response->success) {
            
                try {
                    $userid = UserDB::APICreateUser($cuc); 
                    $custm = array();
                    $custm['userid'] = $userid;
                    $custm['street_address'] = $user['street_address'];
                    $custm['city'] = $user['city'];
                    $custm['postal_code'] = $user['postal_code'];
                    $custm['phone_number'] = $user['phone_number'];

                    $customerid = UserDB::APICreateCustomer($custm);
                    if($customerid >= 0){ //Login Ok
                        echo "Registration success.";
                        $_SESSION["authenticated"] = "true";
                        $_SESSION["userid"] = $userid;                        
                        $_SESSION['username'] = $user["username"]; 
                    }
                    else{ 
                        echo "Username or email already in use.";
                    }
                    http_response_code(201);
                } catch (Exception $exc) {
                    echo returnError(400, $exc->getMessage());
                }
                }
                else { echo 'Wrong captcha response!'; }
            } else { echo 'Captcha response is missing!'; }
        }
        else{
            echo "Data not provided.";
        }
        break;
     case "user":
        if ($http_method == "GET" && $param == null) {           
          if($_SESSION["authenticated"] == true){
              $arr = array();
              $arr["userid"]= $_SESSION["userid"];
              echo json_encode(UserDB::APIgetById($arr));
          }
          else{
              "Not authenticated.";
          }
        }          
        else if ($http_method == "POST" && $param == null) {            
            $filteredInput = filter_input_array(INPUT_POST, $rules);
            $user = array_filter($filteredInput);         
            $user['password'] = hash("sha512", $user['password']);
            
            try {
                $userdata = UserDB::ApiLoginCheck($user);                    
                if(array_key_exists(0, $userdata) >= 1){ //Login Ok
                    echo "Login Ok.";
                    $_SESSION["authenticated"] = "true";
                    if($userdata[0]["id_role"] == 3){
                        $_SESSION["isCustomer"] = true;
                    }
                    else if($userdata[0]["id_role"] == 2){
                        $_SESSION["isSeller"] = true;
                    }
                    else if($userdata[0]["id_role"] == 1){
                        $_SESSION["isAdmin"] = true;
                    }
                    $_SESSION["userid"] = $userdata[0]["id_user"];
                    $_SESSION['username'] = $userdata[0]["user_name"];  
                    http_response_code(200);
                }
                else{ 
                    echo "Wrong username or password.";
                    http_response_code(201);
                }                
            } catch (Exception $exc) {
                 echo returnError(400, $exc->getMessage());
                
            }
        } 
        else {
            // error
            echo returnError(404, "Unknown request: [$http_method $resource]");
        }
        break;
    default:
        returnError(404, "Invalid resource: " . $resource);
        break;
}

