<?php

require_once 'AbstractDB.php';

class ProductDB extends AbstractDB {

    public static function insert(array $params) {
        return parent::modify("INSERT INTO product (id_category, `name`, manufacturer, description"
            . ", short_description, price, available) "
            . " VALUES (:id_category, :product_name, :manufacturer, :description,"
                . " :short_description, :price, :available);"
                . " SELECT LAST_INSERT_ID();", $params);
    }

    public static function update(array $params) {
        return parent::modify("UPDATE product SET name = :product_name, manufacturer = :manufacturer, "
            . "description = :description, short_description = :short_description, available = :available, price = :price WHERE id_product = :id_product", $params);
    }

    public static function delete(array $id) {
        return parent::modify("DELETE FROM product WHERE id_product = :id_product", $id);
    }

    public static function get(array $params) {
        return parent::query("SELECT p.id_product, p.id_category, p.name as product_name, p.manufacturer, p.description"
            . ", p.short_description, p.price, p.available, i.name, i.path"
            . " FROM product p"
            . " INNER JOIN image i ON p.id_product = i.id_product"
            . " WHERE p.id_product = :id_product", $params);
    }
     
    public static function getAllByCategory(array $params) {
          return parent::query("SELECT p.id_product, p.id_category, p.name as product_name, p.manufacturer, p.description"
            . ", p.short_description, p.price, p.available, i.name, i.path"
            . " FROM product p"
            . " INNER JOIN image i ON p.id_product = i.id_product"
            . " WHERE p.id_category = :id_category"
            . " ORDER BY p.id_product ASC", $params);
    }

    public static function getAll() {
                return parent::query("SELECT p.id_product, p.id_category, p.name as product_name, p.manufacturer, p.description"
            . ", p.short_description, p.price, p.available, i.name, i.path"
            . " FROM product p"
            . " INNER JOIN image i ON p.id_product = i.id_product"
            . " ORDER BY p.id_product ASC");
    } 

    public static function getAllDetails() {
        return parent::query("SELECT id, author, title, description, price "
            . " FROM book"
            . " ORDER BY author ASC");
    }

    public static function getAllByType(array $params) {
         return parent::query("SELECT *"
            . " FROM product"
            . " WHERE id_category = :id_category", $params);
    }

}
