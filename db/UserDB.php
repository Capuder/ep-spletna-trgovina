<?php

require_once 'AbstractDB.php';

class UserDB extends AbstractDB {

       
    public static function insert(array $params) {
        return parent::modify("INSERT INTO user (id_role, user_name, password, first_name, last_name"
            . ", `e-mail`) "
            . " VALUES (:id_role, :user_name, :password, :first_name, :last_name, :email)", $params);
    }
    
     public static function APICreateUser(array $params) {
        return parent::modify("INSERT INTO user (id_role, user_name, password, first_name, last_name"
            . ", `e-mail`, confirmed) "
            . " VALUES (:idrole, :username, :password, :first_name, :last_name, :email, 1)", $params);
    }
    public static function APICreateCustomer(array $params) {
        return parent::modify("INSERT INTO customer (id_user, street_address, city, postal_code, phone_number) "
            . " VALUES (:userid, :street_address, :city, :postal_code, :phone_number)", $params);
    }
    
    public static function APIUpdatePassword(array $params) {
        return parent::modify("UPDATE `user` SET `password` = :password WHERE `user`.`id_user` = :userid", $params);
    }
    
    public static function UpdateUserActive(array $params) {
        return parent::modify("UPDATE `user` SET `confirmed` = :status WHERE user_name = :username", $params);
    }
    
    public static function APIUpdateUser(array $params) {
        return parent::modify(
              "UPDATE user SET first_name = :first_name, last_name = :last_name, "
            . "`e-mail` = :email WHERE id_user = :userid ", $params);
    }

      public static function APIUpdateCustomer(array $params) {
        return parent::modify(
              "UPDATE customer SET street_address = :street_address, "
            . "city = :city, postal_code = :postal_code, phone_number = :phone_number WHERE id_user = :userid ", $params);
    }  
    
    public static function delete(array $id) {
        return parent::modify("DELETE FROM user WHERE id_user = :id", $id);
    }

    public static function get(array $params) {
        return parent::query("SELECT *"
            . " FROM user"
            . " WHERE user_name = :username"
            . " LIMIT 1", $params);
    }
    
    public static function APIgetById(array $params) {
        return parent::query("SELECT user_name, first_name, last_name, `e-mail` "
            . " FROM user"
            . " WHERE id_user = :userid ", $params);
    }
    
    public static function getAllByType(array $params) {
        return parent::query("SELECT `Image`, `Name`, `Character`"
            . " FROM Images"
            . " WHERE GameType = :gameType", $params);
    }

    public static function getAllCustomer(array $params) {
        return parent::query("SELECT u.user_name, u.first_name, u.last_name, u.`e-mail`, c.street_address, c.city, c.postal_code, c.phone_number
FROM user u 
INNER JOIN customer c 
ON u.id_user = c.id_user
WHERE c.id_user = :userid ", $params);
    }
public static function getIdFromUsername(array $params) {
        return parent::query("SELECT id_user FROM user"
            . " WHERE user_name = :username ", $params);
    }
    public static function getAllDetails() {
        
    }
    
    public static function ApiLoginCheck(array $params){
        return parent::query("SELECT *"
            . " FROM user"
            . " WHERE user_name = :username && password= :password"
            . " LIMIT 1", $params);
    }

    public static function getCategories() {
        
    }

    public static function getAllSellers(){
        return parent::query("SELECT * "
            . " FROM user"
            . " WHERE id_role = 2");
    }
    public static function getAllCustomers(){
        return parent::query("SELECT * "
            . " FROM user"
            . " WHERE id_role = 3");
    }
    public static function getAll() {
        
    }

    public static function update(array $params) {
        
    }

}
