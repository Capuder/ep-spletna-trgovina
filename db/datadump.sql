
INSERT INTO `product` (`id_product`, `id_category`, `name`, `manufacturer`, `description`, `short_description`, `stock`, `in_stock`, `price`, `available`) VALUES
(0, 1, 'Kolo Mach1', 'Gorenje', '<p>Super Kolo<p>', 'Super kolo za v gore', 9, 1, 999, 1),
(1, 2, 'Kolo Mach2', 'Gorenje', '<p>Super Kolo<p>', 'Super kolo za po cesti', 9, 1, 999.99, 1),
(2, 1, 'Kolo Mach2', 'Gorenje', '<p>Super Kolo<p>', 'Super kolo za v gore', 9, 1, 999, 1),
(3, 2, 'Kolo Mach3', 'Gorenje', '<p>Super Kolo M3<p>', 'Super kolo za po cesti', 9, 1, 666, 1),
(4, 2, 'Kolo Mach4', 'Gorenje', '<p>Super Kolo<p>', 'Super kolo za po cesti', 9, 1, 1201, 1);

--
-- Dumping data for table `product_category`
--

INSERT INTO `product_category` (`id_category`, `name`, `description`) VALUES
(1, 'gorska kolesa', 'Vsa gorska kolesa'),
(2, 'cestna kolesa', 'Vsa cestna kolesa');
