<?php

require_once 'AbstractDB.php';

class ImageDB extends AbstractDB {

    public static function insert(array $params) {
        return parent::modify("INSERT INTO image (id_product, `name`, path)"
            . " VALUES (:id_product, :name, :path)", $params);
    }

    public static function update(array $params) {
        return parent::modify("UPDATE product SET author = :author, title = :title, "
            . "description = :description, price = :price WHERE id_product = :id", $params);
    }

    public static function delete(array $id) {
        return parent::modify("SET FOREIGN_KEY_CHECKS = 0; DELETE FROM product WHERE id_product = :id_product", $id);
    }

    public static function get(array $id) {
        return parent::query("SELECT *"
            . " FROM image"
            . " WHERE id_product = :id_product", $id);
    }
     
    public static function getAllByCategory(array $params) {
        return parent::query("SELECT *"
            . " FROM product"
            . " WHERE id_category = :id_category", $params);
    }

    public static function getAll() {
        return parent::query("SELECT *"
            . " FROM product"
            . " ORDER BY id_product ASC");
    } 

    public static function getAllDetails() {
        return parent::query("SELECT id, author, title, description, price "
            . " FROM book"
            . " ORDER BY author ASC");
    }

    public static function getAllByType(array $params) {
         return parent::query("SELECT *"
            . " FROM product"
            . " WHERE id_category = :id_category", $params);
    }

}
