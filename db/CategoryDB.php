<?php

require_once 'AbstractDB.php';

class CategoryDB extends AbstractDB {    
    public static function getAll() {
                return parent::query("SELECT *"
                . " FROM product_category"
                . " ORDER BY id_category ASC");
    }

    public static function delete(array $id) {
        
    }

    public static function get(array $id) {

    }

    public static function getAllByType(array $params) {
        
    }
    public static function insert(array $params) {
        
    }

    public static function update(array $params) {
        
    }

}
