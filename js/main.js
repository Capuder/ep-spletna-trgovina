$( document ).ready(function() {
        $('#add_to_basket').click(function(){
            var id_product = $(this).attr('id_product');
                    $.ajax({
                           method: "POST",
                           url: "inc/add_to_basket.php",
                           data: { id_product: id_product },
                           success: function(msg){
                              alert(msg);
                           },
                           error: function(msg){
                              alert(msg);
                           }
                           });
        });
         $('#deleteProduct').click(function(){
             alert('delete!');
            var id_product = $(this).attr('id_product');
            if(confirm('Ali res želite pobrisati produkt?')){
            $(this).parent().parent().remove();
                    $.ajax({
                           method: "POST",
                           url: "inc/delete_product.php",
                           data: { id_product: id_product },
                           success: function(msg){
                              alert(msg);
                           },
                           error: function(msg){
                              alert(msg);
                           }
                           });
             }
        });
});

function registerValidate(){

    $('.form-control').each(function(){
            if($(this).val().length < 1){
                    $(this).addClass('error');
                    isValid = false;
            }
            else{
                    isValid = true;
                    $(this).removeClass('error');
            }
    });
    if ($('#password').val() !== $('#repeat-password').val()){
        $('div.notification').text('Gesli se morata ujemati.');
        isValid = false;
    }
    if(grecaptcha.getResponse().length == 0) isValid = false;
        
    if(!isValid){
        $('div.notification').text('Vsa polja morajo biti izpolnjena.');
    }
    else {
        $.ajax({
                    method: "POST",
                    url: "api/user_reg",                                                        
                    data: $('#registration').serialize() ,
                    success: function(data){
                           $('div.notification').text(data.responseText);  
                    },
                    error: function(data){
                           $('div.notification').text(data.responseText);                                                               
                    }
                    });
    }
    
    return false;
}