<footer class="site-footer">
	<div class='fmenu'>
        	<a href="/logout">logout</a>	
	</div>
	
	<br>

	<p>Admin Panel - <?php echo $sitename; ?></p>
        <script type="text/javascript" src="../js/jquery.js"></script>
        <script src="../js/jquery-ui.min.js"></script>
        <!-- Bootstrap jQuery -->
        <script src="../js/bootstrap.min.js"></script>
        <script src='https://www.google.com/recaptcha/api.js'></script>
        <script src="../ckeditor/ckeditor.js"></script>
        <script src="../admin/js/adminjs.js"></script>
        <script>
              CKEDITOR.replace('description');
        </script>
</footer>