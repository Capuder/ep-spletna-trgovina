<?php 
include 'inc/checkadmin.php'; 
?>  
  
<div class="row"> 
	<div class="col-md-4"></div>
	<div class="col-md-4" style="top:10px">
            <h1>KREIRANJE PRODAJALCA</h1>
                        <div class="form-group">
				<label for="first_name">Ime</label>
				<input type="text" class="form-control" name="first_name" id="first_name" placeholder="Vpišite vaše ime" />
			</div>
                        <div class="form-group">
				<label for="last_name">Priimek</label>
				<input type="text" class="form-control" name="last_name" id="last_name" placeholder="Vpišite vaš priimek" />
			</div>
                       
                        <div class="form-group">
				<label for="email">Elektronski naslov</label>
				<input type="text" class="form-control" name="email" id="email" placeholder="Vpišite vaš elektronski naslov" />
			</div>
			<div class="form-group">
				<label for="username">Uporabniško ime</label>
				<input type="text" class="form-control" name="username" id="username" placeholder="Vpišite vaše uporabniško ime" />
			</div>
                        <div class="form-group">
			<label for="password">Geslo</label>
				<input type="password" class="form-control" name="password" id="password" onkeypress="Check(this.id, 6);"  placeholder="Vpišite geslo" />
			</div>
                        <div class="form-group">
				<label for="password">Ponovite geslo</label>
				<input type="password" class="form-control" name="password-repeat" id="password-repeat"  placeholder="Vpišite geslo" />
			</div>            
                        <div class="form-group">
                        <div class="notification"></div>	
                        </div>            
			<div class="form-group">
                        <button class="btn btn-default" onclick="AjaxCall()">Potrdi registracijo</button>
                        </div>
                        
                       			
	</div>	
</div>
</div>
<script>
    function AjaxCall(){   
        
            $.ajax({
							method: "POST",
							url: "actions/createseller.php",                                                        
							data: { username: $('#username').val(),
                                                                password: $('#password').val(),
                                                                first_name: $('#first_name').val(),
                                                                last_name: $('#last_name').val(),
                                                                email: $('#email').val()
                                                            },
							success: function(data){
								$('div.notification').text(data.responseText);  
							},
							error: function(data){
                                                               $('div.notification').text(data.responseText);                                                               
							}
							});
                                                        
                                                    };
                                                     
</script>