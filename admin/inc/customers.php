<?php

$users = UserDB::getAllCustomers()

?>
<div id="myModal" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                            <h3 id="myModalLabel">Delete</h3>
                        </div>
                        <div class="modal-body">
                            <p></p>
                         </div>
                         <div class="modal-footer">
                            <button class="btn" data-dismiss="modal" aria-hidden="true">Close</button>
                            <button data-dismiss="modal" class="btn red" id="btnYes">Confirm</button>
                        </div>
   </div><table class="table table-striped table-hover table-users">
    			<thead>
    				<tr>					
    					<th>Ime</th>
    					<th>Priimek</th>
    					<th>Uporabniško ime</th>
    					<th>Email</th>
    					<th>Status</th>
    					<th>Vrsta uporabnika</th>
                                        <th></th>
    					<th></th>
    					<th></th>
    				</tr>
    			</thead>

    			<tbody>
            <?php foreach($users as $u){ ?>	
                                    <tr>
                        
    					<td><?php echo $u['first_name'] ?></td>
    					<td><?php echo $u['last_name'] ?></td>
    					<td><?php echo $u['user_name'] ?></td>
    					<td><?php echo $u['e-mail'] ?></td>
                                        <td><?php if ($u['confirmed']) echo '<span class="label label-warning">Potrjen'; else echo '<span class="label label-danger">Nepotrjen</span>' ?></td>
                                        <td><?php echo $u['role_name'] ?></td>
    					   					
                    	  		<td><?php if ($u['confirmed'])
                                            echo '<a class="btn mini blue-stripe" href="javascript:ChangeConfirmed(`' . $u['user_name'] . '`, 0)">Deaktiviraj</a>'; 
                                       else echo '<a class="btn mini blue-stripe" href="javascript:ChangeConfirmed(`' . $u['user_name'] . '`, 1)">Potrdi</a>'; ?></td>			
                                        <td><a class="btn mini blue-stripe" href="modifycustomer.php?username=<?php echo $u['user_name'];?>">Uredi</a></td>

                        <td><a href="#" class="confirm-delete btn mini red-stripe" role="button" data-title="kitty" data-id="2">Izbriši</a></td>
                    </tr>
            <?php } ?>
                    </tbody>

             </table>