<?php

$categories = CategoryDB::getAll();
$categoriesSelect = '<select id="product_category" name="product_category">';
foreach ($categories as $cat){
    $categoriesSelect .= '<option value="' . $cat['id_category'] . '">' . $cat['name'] . '</value>';
}
$categoriesSelect .= '</select>';

?>

<div class="row"> 
    <div class="col-md-3"></div>
        <div class="col-md-6">
            <h2>Dodaj produkt</h2>
            <form id="add_product" name="add_product" role="form" method="post" action="actions/add_product.php" enctype="multipart/form-data">
                <div class="form-group">
                    <label for="product_name">Ime izdelka</label>
                    <input type="text" class="form-control" name="product_name" id="product_name" placeholder="Vpišite ime izdelka" />
                </div>
                <div class="form-group">
                    <label for="manufacturer">Ime proizvajalca</label>
                    <input type="text" class="form-control" name="manufacturer" id="manufacturer" placeholder="Vpišite ime proizvajalca" />
                </div>
                <div class="form-group">
                    <label for="product_category">Kategorija</label><br />
                    <?php echo $categoriesSelect; ?>
                </div>
                 <h3>Opis izdelka</h3>
                 <div class="form-group">
                    <textarea name="description" id="description" rows="20" cols="80">
                    </textarea>
                 </div>
                 <br />
                 <h3>Kratek opis</h3>
                 <div class="form-group">
                    <textarea name="short_description" id="short_description" rows="20" cols="80">
                    </textarea>
                 </div>
                 <h3>Na voljo</h3>
                 <div class="form-group">
                    <input type="radio" name="available" value="true"> Da<br>
                    <input type="radio" name="available" value="false"> Ne<br>
                 </div>
                 <h3>Cena</h3>
                 <div class="form-group">
                    <label for="price">Cena</label>
                    <input type="text" class="form-control" name="price" id="price" placeholder="Vpišite ceno" />
                 </div>
                <div class="form-group">
                    <label for="image">Izberite sliko</label>
                    <input type="file" class="form-control" name="image" id="image" >
                </div>
                <div class="form-group">
                    <input type="submit" class="btn btn-default" value="Potrdi" name="addProduct" />
                </div>
            </form>
        </div>
    <div class="col-md-3"></div>
</div>