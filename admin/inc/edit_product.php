<?php

$params['id_product'] =  $_GET['id'];
$product = ProductDB::get($params);

?>

<div class="row"> 
    <div class="col-md-3"></div>
        <div class="col-md-6">
            <h2>Uredi produkt</h2>
            <form id="add_product" name="add_product" role="form" method="post" action="actions/edit_product.php" enctype="multipart/form-data">
                <div class="form-group">
                    <label for="product_name">Ime izdelka</label>
                    <input type="text" class="form-control" name="product_name" id="product_name" value="<?php echo $product[0]['product_name']; ?>" placeholder="Vpišite ime izdelka" />
                </div>
                <div class="form-group">
                    <label for="manufacturer">Ime proizvajalca</label>
                    <input type="text" class="form-control" name="manufacturer" id="manufacturer" value="<?php echo $product[0]['manufacturer']; ?>" placeholder="Vpišite ime proizvajalca" />
                </div>
                 <h3>Opis izdelka</h3>
                 <div class="form-group">
                    <textarea name="description" id="description" rows="20" cols="80"><?php echo $product[0]['description']; ?>
                    </textarea>
                 </div>
                 <br />
                 <h3>Kratek opis</h3>
                 <div class="form-group">
                    <textarea name="short_description" id="short_description" rows="20" cols="80"><?php echo $product[0]['short_description']; ?>
                    </textarea>
                 </div>
                 <h3>Na voljo</h3>
                 <div class="form-group">
                    <input type="radio" name="available" <?php if ($product[0]['available'] == 1) echo 'checked'; ?> value="1"> Da<br>
                    <input type="radio" name="available" <?php if ($product[0]['available'] == 0) echo 'checked'; ?> value="0"> Ne<br>
                 </div>
                 <div class="form-group">
                    <label for="price">Cena</label>
                    <input type="text" class="form-control" name="price" id="price" placeholder="Vpišite ceno" value="<?php echo $product[0]['price']; ?>" />
                 </div>
                 <img src="<?php echo $product[0]['path']; ?>" style="max-height: 300px;" />
                <!--div class="form-group">
                    <label for="image">Izberite sliko, če jo želite zamenjati</label>
                    <input type="file" class="form-control" name="image" id="image" >
                </div-->
                <input type="hidden" id="id_product" name="id_product" value="<?php echo $product[0]['id_product']; ?>" />
                <div class="form-group">
                    <input type="submit" class="btn btn-default" value="Potrdi" name="editProduct" />
                </div>
            </form>
        </div>
    <div class="col-md-3"></div>
</div>