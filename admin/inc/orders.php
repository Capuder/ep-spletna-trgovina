                   <div id="myModal" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                            <h3 id="myModalLabel">Delete</h3>
                        </div>
                        <div class="modal-body">
                            <p></p>
                        </div>
                        <div class="modal-footer">
                            <button class="btn" data-dismiss="modal" aria-hidden="true">Close</button>
                            <button data-dismiss="modal" class="btn red" id="btnYes">Confirm</button>
                        </div>
   </div><table class="table table-striped table-hover table-users">
    			<thead>
    				<tr>					
    					<th>Številka naročila</th>
    					<th>Stranka</th>
    					<th>Uporabniško ime</th>
    					<th>Email</th>
    					<th>Status</th>
    					<th>Vrsta uporabnika</th>
                                        <th>Uporabnik od</th>
    					<th></th>
    					<th></th>
    				</tr>
    			</thead>

    			<tbody>
    				
    				<tr>
                        
    					<td>Nika</td>
    					<td>Brglez</td>
    					<td>Nika</td>
    					<td>nika.brglez@gmail.com</td>
    					<td><span class="label label-warning">Potrjen</span></td>
                                        <td>Prodajalec</td>
                                        <td>10/12/2015</td>
                    	
                    	  					
    					<td><a class="btn mini blue-stripe" href="{site_url()}admin/editFront/1">Edit</a></td>

                        <td><a href="#" class="confirm-delete btn mini red-stripe" role="button" data-title="johnny" data-id="1">Delete</a></td>
                    </tr>
					<tr>
                        
    					<td>Jan</td>
    					<td>Capuder</td>
    					<td>jcapuder</td>
    					<td>capuder.jan@gmail.com</td>
                                        <td><span class="label label-danger">Nepotrjen</span></td>
                                        <td>Prodajalec</td>
                                        <td>10/1/2015</td>
    					   					
                    	  					
    					<td><a class="btn mini blue-stripe" href="{site_url()}admin/editFront/2">Edit</a></td>

                        <td><a href="#" class="confirm-delete btn mini red-stripe" role="button" data-title="kitty" data-id="2">Delete</a></td>
                    </tr>
                
	               </tbody>

    		</table>