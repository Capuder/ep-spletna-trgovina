<?php

$products = ProductDB::getAll()

?>

<div id="myModal" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                            <h3 id="myModalLabel">Delete</h3>
                        </div>
                        <div class="modal-body">
                            <p></p>
                        </div>
                        <div class="modal-footer">
                            <button class="btn" data-dismiss="modal" aria-hidden="true">Close</button>
                            <button data-dismiss="modal" class="btn red" id="btnYes">Confirm</button>
                        </div>
   </div><table class="table table-striped table-hover table-users">
    			<thead>
    				<tr>					
    					<th>Ime produkta</th>
    					<th>Proizvajalec</th>
    					<th>Cena</th>
    					<th>Na voljo</th>
    					<th></th>
    					<th></th>
    				</tr>
    			</thead>

    			<tbody>
    	<?php foreach($products as $p){ ?>			
    				<tr>
                        
                                <td class="hidden-phone"><?php echo $p['product_name']; ?></td>
                                <td><?php echo $p['manufacturer']; ?></td>
                                <td><?php echo $p['price']; ?></td>
                                <td class="hidden-phone"><?php if ($p['available']) echo 'Na voljo'; else echo 'Ni na voljo' ?></td>
    				<td><a class="btn mini blue-stripe" href="edit_product?id=<?php echo $p['id_product']; ?>">Uredi</a></td>

                        <td><a href="javascript:void(0)" id_product="<?php echo $p['id_product']; ?>" class="confirm-delete btn mini red-stripe deleteProduct" role="button" data-title="johnny" data-id="1">Izbriši</a></td>
                    </tr>
        <?php } ?>        
                    </tbody>

             </table>