<?php
    //include 'inc/checkadmin.php';
    include '../../db/ProductDB.php';
    include '../../db/ImageDB.php';
    
    if (isset($_POST['addProduct'])){
      $params['id_category'] = $_POST['product_category'];
      $params['product_name'] = $_POST['product_name'];
      $params['manufacturer'] = $_POST['manufacturer'];
      $params['description'] = $_POST['description'];
      $params['short_description'] = $_POST['short_description'];
      $params['price'] = $_POST['price'];
      $params['available'] = $_POST['available'];
      
      $id_product = ProductDB::insert($params);
      
      echo 'Produkt je bil uspešno dodan.';
        
      if(isset($_FILES['image'])){
        $target_dir = '../../images/products/';
        $target_file = $target_dir . basename($_FILES['image']['name']);
        $uploadOk = 1;
        $imageFileType = strtolower(pathinfo($target_file, PATHINFO_EXTENSION));
        $check = getimagesize($_FILES['image']['tmp_name']);
        if ($check !== false) {
            $uploadOk = 1;
        } else {
            echo 'Datoteka ni slika.';
            $uploadOk = 0;
        }
    // Check if file already exists
     //   if (file_exists($target_file)) {
     //       echo 'Ta slika že obstaja.';
     //       $uploadOk = 0;
     //   }
    // Check file size
        if ($_FILES['image']['size'] > 50000000) {
            echo 'Datoteka je prevelika';
            $uploadOk = 0;
        }
    // Allow certain file formats
        if ($imageFileType != 'jpg' && $imageFileType != 'png' && $imageFileType != 'jpeg'
            && $imageFileType != 'gif'
        ) {
            echo 'Napačen format datoteke! Dovoljeni samo .jpg, .png in .gif.';
            $uploadOk = 0;
        }
    // Check if $uploadOk is set to 0 by an error
        if ($uploadOk == 0) {
            echo 'Datoteka ni bila naložena.';
    // if everything is ok, try to upload file
        } else {
            if (move_uploaded_file($_FILES['image']['tmp_name'], $target_file)) {
                // zapiši v bazo
                $params = array();
                $params['name'] = 'image_' . $id_product;
                $params['path'] = $target_file;
                $params['id_product'] = $id_product;
                ImageDB::insert($params);
                echo 'Slika uspešno naložena v mapo ' . $target_dir;
            } else {
                echo 'Datoteka ni bila naložena.';
            }
        }
      }
        
        }
        
      header("Location: /admin/products");
      die();
   
?>