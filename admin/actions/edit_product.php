<?php
    //include 'inc/checkadmin.php';
    include '../../db/ProductDB.php';
    //include '../../db/ImageDB.php';
    
    if (isset($_POST['editProduct'])){
      $params['product_name'] = $_POST['product_name'];
      $params['manufacturer'] = $_POST['manufacturer'];
      $params['description'] = $_POST['description'];
      $params['short_description'] = $_POST['short_description'];
      $params['price'] = $_POST['price'];
      $params['available'] = $_POST['available'];
      $params['id_product'] = $_POST['id_product'];
     
      ProductDB::update($params);
      
      echo 'Produkt je bil uspešno posodobljen.';
        
      header("Location: /admin/products");
      die();
        
     }
   
?>