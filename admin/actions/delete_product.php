<?php
include '../../db/ImageDB.php';
include '../../db/ProductDB.php';
    
if(isset($_POST['id_product'])){
    $params['id_product'] = $_POST['id_product'];
    ImageDB::delete($params);
    ProductDB::delete($params);
    echo('Produkt je bil uspešno zbrisan.');
}
else {
    echo('Manjka POST parameter za ID produkta.');
}