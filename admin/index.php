<!DOCTYPE html>
<?php 
      include '../inc/config.php';
//     include 'inc/checkadminorseller.php';
      include '../db/UserDB.php'; 
      include '../db/ProductDB.php';
      include '../db/CategoryDB.php';
      require_once '../inc/functions.php';
      
    $path = str_replace('-', '_', contains_value($absPath, $adminPaths));
    
?>
<!DOCTYPE html>
<html lang="en">

<?php include 'inc/head.php' ?>

<body>

    <div id="wrapper">

        <!-- Navigation -->
        <nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
            <!-- Brand and toggle get grouped for better mobile display -->
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-ex1-collapse">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="/admin">Spletna trgovina - admin panel</a>
            </div>
            <!-- Top Menu Items -->
            <ul class="nav navbar-right top-nav">
                <li class="dropdown">
                    
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                        <i class="fa fa-user"></i> 
                            <?php if(isset($_SESSION['username'])){ echo $_SESSION['username'];} else { echo 'Gost';}?>
                            <?php if(isset($_SESSION['isAdmin'])){ echo "[Administrator]";} ?>
                            <?php if(isset($_SESSION['isSeller'])){ echo "[Prodajalec]";} ?><b class="caret"></b></a>
                   
                            <ul class="dropdown-menu">
                       
                        <?php if(isset($_SESSION['username'])){ ?>
                        <li>
                            <a href="#"><i class="fa fa-fw fa-user"></i> Profile</a>
                        </li>
                        <li>
                            <a href="#"><i class="fa fa-fw fa-envelope"></i> Inbox</a>
                        </li>
                        <li>
                            <a href="/admin/modifyuser"><i class="fa fa-fw fa-gear"></i> Settings</a>
                        </li>
                        <li>
                            <a href="/admin/changepass"><i class="fa fa-fw fa-gear"></i> Change password</a>
                        </li>
                        <li class="divider"></li>
                        <li>
                            <a href="/logout"><i class="fa fa-fw fa-power-off"></i> Log Out</a>
                        </li>
                        <?php } else { ?>
                        <li>
                            <a href="/admin/login"><i class="fa fa-fw fa-power-off"></i> Login</a>
                        </li>
                        <?php } ?>
                    </ul>
                </li>
            </ul>
            
            <!-- Sidebar Menu Items - These collapse to the responsive navigation menu on small screens -->
            <div class="collapse navbar-collapse navbar-ex1-collapse">
                <ul class="nav navbar-nav side-nav">
                    <li class="active">
                        <a href="index"><i class="fa fa-fw fa-dashboard"></i> Nadzorna plošča</a>
                    </li>
                    <li>
                        <a href="new_product"><i class="fa fa-fw"></i> Dodaj produkt</a>
                    </li>
                    <li>
                        <a href="products"><i class="fa fa-fw"></i> Seznam produktov</a>
                    </li>
                    <li>
                        <a href="orders"><i class="fa fa-fw"></i> Seznam naročil</a>
                    </li>
                    <li>
                        <a href="users"><i class="fa fa-fw"></i> Seznam prodajalcev</a>
                    </li>
                    <li>
                        <a href="customers"><i class="fa fa-fw"></i> Seznam strank</a>
                    </li>
                    <li>
                        <a href="createseller"><i class="fa fa-fw"></i> Dodaj prodajalca</a>
                    </li>
                    <li>
                        <a href="/register"><i class="fa fa-fw"></i> Dodaj kupca</a>
                    </li>
                </ul>
            </div>
            <!-- /.navbar-collapse -->
        </nav>

        <div id="page-wrapper">

                <div class="row">
                <?php
                     $content = 'inc/intro.php';

                        if ($path !== '') $content = 'inc/' . $path . '.php';

                        include $content;
                        
                ?>
                    
                </div>
                <!-- /.row -->

            </div>
            <!-- /.container-fluid -->

        </div>
        <!-- /#page-wrapper -->

    </div>
    <!-- /#wrapper -->

    <?php include 'inc/footer.php' ?>

</body>

</html>
